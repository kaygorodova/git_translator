#ifndef grammar_h
#define grammar_h
#include <iostream>
#include <string>
#include <vector>
#include "lexer.h"
#include "table.h"




class ExprAST;
class Visitor;

class ExprAST {

public:
	virtual ~ExprAST() {}
	virtual void printTag(ostream &out);
	virtual void printAttrib(ostream &out);
	virtual void createTable(Table *t);
	virtual int getInt();
	virtual string getName();
	virtual TypeNumber getTypeNumber(Table *t);
	virtual TypeStruct getTypeStruct(Table *t);
	virtual void accept(Visitor &v);


	int getNumberLine(){
		return number_line;
	}

protected:
	int number_line;
};

class IntExprAST : public ExprAST {
public:
	int val;
	IntExprAST(int val, int number_line){
		this->val = val;
		this->number_line = number_line;
	}

	void printTag(ostream &out);
	void printAttrib(ostream &out);
	int getInt();
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	void accept(Visitor &v);
	
};

class RealExprAST : public ExprAST {
public:
	double val;
	RealExprAST(double val, int number_line){
		this->val=val;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	void accept(Visitor &v);
	
};

class IDExprAST : public ExprAST {
public:
	string name;
	IDExprAST(string name, int number_line){
		this->name=name;
		this->number_line = number_line;
	}

	void printAttrib(ostream &out);
	string getName();
	TypeNumber getTypeNumber(Table *t);
	void accept(Visitor &v);
};

class VarExprAST : public ExprAST{
public:
	ExprAST* id;
	ExprAST* index;
	VarExprAST(ExprAST* id, ExprAST* index, int number_line){
		this->id = id;
		this->index = index;
		this->number_line = number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	TypeNumber getTypeNumber();
	string getName();
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	void accept(Visitor &v);
};

class LabelExprAST : public ExprAST{
public:
	ExprAST* id;
	LabelExprAST(ExprAST* id, int number_line){
		this->id=id;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class GoToExprAST : public ExprAST{
public:
	ExprAST* id;
	GoToExprAST(ExprAST* id, int number_line){
		this->id=id;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class ReadExprAST : public ExprAST{
public:
	vector<ExprAST*> vars;
	ReadExprAST(vector<ExprAST*> vars, int number_line){
		this->vars=vars;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class OperandExprAST : public ExprAST{
public:
	ExprAST *operand;
	OperandExprAST(ExprAST* operand, int number_line){
		this->operand=operand;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	string getName();
	//Type getType(Table &tableName);
	void accept(Visitor &v);
};

class BinaryExprAST : public ExprAST{
public:
	string op;
	ExprAST *l_operand, *r_operand;
	BinaryExprAST(string op, ExprAST *l_operand, ExprAST *r_operand, int number_line){
		this->op=op;
		this->l_operand=l_operand;
		this->r_operand=r_operand;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	void accept(Visitor &v);
};

class NegationExprAST : public ExprAST {
public:
	ExprAST *operand;
	NegationExprAST(ExprAST *operand, int number_line){
		this->operand=operand;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	TypeNumber getTypeNumber(Table *t);
	TypeStruct getTypeStruct(Table *t);
	void accept(Visitor &v);
};


class MovExprAST : public ExprAST {
public:
	ExprAST* var;
	ExprAST* expr;
	MovExprAST(ExprAST* expr, ExprAST* var, int number_line){
		this->var=var;
		this->expr=expr;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class WriteExprAST : public ExprAST {
public:
	vector<ExprAST*> args;
	vector<bool> fl;
	WriteExprAST(vector<ExprAST* > args, vector<bool> fl, int number_line){
		this->args=args;
		this->fl = fl;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class QualifierExprAST : public ExprAST {
public:
	string name;
	QualifierExprAST(string name, int number_line){
		this->name=name;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void accept(Visitor &v);
};

class BreakExprAST : public ExprAST {
public:
	BreakExprAST(int number_line){
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class CallExprAST : public ExprAST {
public:
	ExprAST* name;
	vector<ExprAST*> args;
	CallExprAST(ExprAST* name, vector<ExprAST*> args, int number_line){
		this->name=name;
		this->args=args;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};


class TypeExprAST : public ExprAST{
public:
	string name;
	TypeExprAST(string name, int number_line){
		this->name=name;
		this->number_line=number_line;
	}

	void printAttrib(ostream &out);
	TypeNumber getTypeNumber(Table *t);
	void accept(Visitor &v);
};

class DescrExprAST : public ExprAST{
public:
	ExprAST* type;
	vector<pair<ExprAST*, ExprAST*> > args;
	DescrExprAST(ExprAST* type, vector<pair<ExprAST*, ExprAST*> > args, int number_line){
		this->type=type;
		this->args=args;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class IfExprAST : public ExprAST{
public:
	ExprAST* expr;
	vector<ExprAST*> yes;
	vector<ExprAST*> no;
	IfExprAST(ExprAST* expr, vector<ExprAST*> yes, vector<ExprAST*> no, int number_line){
		this->expr=expr;
		this->yes=yes;
		this->no=no;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class CycleExprAST : public ExprAST {
public:
	vector<ExprAST*> body;
	CycleExprAST(vector<ExprAST*> body, int number_line){
		this->body=body;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class ComponentExprAST : public ExprAST {
public:
	vector<ExprAST*> body;
	ComponentExprAST(vector<ExprAST*> body, int number_line){
		this->body=body;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class OperatorExprAST : public ExprAST {
public:
	ExprAST* label;
	ExprAST* unlabeled;
	OperatorExprAST(ExprAST* label, ExprAST* unlabeled, int number_line){
		this->label=label;
		this->unlabeled=unlabeled;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class FunctionAST : public ExprAST{
public:
	ExprAST *id;
	vector<ExprAST*> args;
	ExprAST *body;	
	FunctionAST(ExprAST *id, vector<ExprAST*> args, ExprAST *body, int number_line){
		this->id=id;
		this->args=args;
		this->body=body;
		this->number_line=number_line;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};

class ProgramAST : public ExprAST{
public:
	vector<ExprAST*> vars;
	vector<ExprAST*> funs;
	ExprAST *body;	
	ProgramAST(vector<ExprAST*> vars, vector<ExprAST*> funs, ExprAST *body){
		this->vars=vars;
		this->funs=funs;
		this->body=body;
	}

	void printTag(ostream &out);
	void createTable(Table *t);
	void accept(Visitor &v);
};


class Visitor{
public:
	virtual void visit(ExprAST *e){ return; }
	virtual void visit(IntExprAST *e){ return; }
	virtual void visit(RealExprAST *e){ return; }
	virtual void visit(IDExprAST *e){ return; }
	virtual void visit(VarExprAST *e){ return; }
	virtual void visit(LabelExprAST *e){ return; }
	virtual void visit(GoToExprAST *e){ return; }
	virtual void visit(ReadExprAST *e){ return; }
	virtual void visit(OperandExprAST *e){ return; }
	virtual void visit(BinaryExprAST *e){ return; }
	virtual void visit(NegationExprAST *e){ return; }
	virtual void visit(MovExprAST *e){ return; }
	virtual void visit(WriteExprAST *e){ return; }
	virtual void visit(QualifierExprAST *e){ return;}
	virtual void visit(BreakExprAST *e){ return; }
	virtual void visit(CallExprAST *e){ return; }
	virtual void visit(TypeExprAST *e){ return; }
	virtual void visit(DescrExprAST *e){ return; }
	virtual void visit(IfExprAST *e){ return; }
	virtual void visit(CycleExprAST *e){ return; }
	virtual void visit(ComponentExprAST *e){ return; }
	virtual void visit(OperatorExprAST *e){ return; }
	virtual void visit(FunctionAST *e){ return; }
	virtual void visit(ProgramAST *e){ return; }
	
};



#endif