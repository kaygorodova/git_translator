#ifndef lexer_h
#define lexer_h
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include "automatic.h"
using namespace std;


struct Lexema{

	int line_number;
	string lexema;
	string type;
	int recognized_value_int;
	double recognized_value_real;
	string value;
};

bool printLexem(vector<Lexema> m, ostream &out);

bool validityOfTokens(vector<Lexema> m);

vector<Automatic> creationAutomatics();

void scanner(string str, int line_number, vector<Lexema> &m);

void error(string val, vector<Lexema> &m, int line_number);

vector<Lexema> getAllLexemas(ifstream &in);




#endif