#ifndef table_h
#define table_h
#include <string>
#include <vector>
//#include "grammar.h"
using namespace std;

class ExprAST;


enum TypeNumber{
	INT,
	REAL,
	NO
};

enum TypeStruct{
	VAR,
	ARRAY,
	FUN,
	LABEL,
	CYCLE,
	PROG,
	OTHER
};

struct Table{
	Table *parent;
	string name;
	TypeNumber type;
	TypeStruct name_struct;
	int ref;
	vector<Table*> child;
	int length;
	bool fl;
	int number_line;
};


Table* contain(Table *t, string name, TypeStruct type, bool check_parent);
Table* contain(Table *t, string name, bool check_parent);
Table* getTable(ExprAST* tree);
void volidTable(Table *t);
bool printTable(int num, Table *t, ostream &out);
string printType(TypeNumber type);

void throwException(int number_line, string mas);
bool validityOfTable(int num, Table *t);



#endif