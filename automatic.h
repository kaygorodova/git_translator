#ifndef automatic_h
#define automatic_h

#include <string>
#include <vector>
#include <iostream>
#define MAX 150
using namespace std;

class Automatic{

	string name;
	int matrix[MAX][MAX];
	int pos;
	int end;


	public:
	Automatic();

	void creationAutomatic(string name);
	void creationAutomaticID();
	void creationAutomaticBinary();
	void creationAutomaticOctal();
	void creationAutomaticDecimal();
	void creationAutomaticHexadecimal();
	void creationAutomaticReal();

	void setName(string name);
	string getName();
	int simulationOfTheAutomaton(string str);

	~Automatic();

	private:
void initializationMatrix();
	void creationMatrix(string name);
};

#endif