#include <iostream>
#include <vector>
#include <fstream>
#include "lexer.h"
#include "parser.h"
#include "grammar.h"
#include "table.h"
#include "code_generation.h"
using namespace std;

int main(int argc, char* argv[])
{
	if(argc < 3){
		cout<<"Error:Params:Incorrect number of parameters"<<endl;
        return 0;
	}

    ifstream in(argv[1], fstream::in);
    ofstream out(argv[2], fstream::out);
	

	if(!in.is_open()){
		cout<<"Error:"<<argv[1]<<":File can not be read"<<endl;
		return 0;
	}

	if(!out.is_open())
    {
        cout<<"Error:"<<argv[2]<<":File can not be written"<<endl;
        return 0;
    }

	

	ExprAST* tree;
	Table *table;
	vector<Command> code;
	

	vector<Lexema> m = getAllLexemas(in);
	if(validityOfTokens(m)){
		tree = getTree(m);
		if(validityOfTree((int)m.size(), tree)){
			table = getTable(tree);
			if(validityOfTable((int)m.size(), table)){
				CreateCode v(table);
				tree->accept(v);
				v.printCode(out);
				cout<<"OK";
			}
		}
	}


	

    return 0;
}
