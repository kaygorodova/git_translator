#include "code_generation.h"
#include "grammar.h"



void CreateCode::printCode(ostream &out){
	int i;
	for(i=0; i<(int)commands.size(); i++)
    {
		switch(commands[i].name)
        {
            case DIV:
                out << "DIV ";
				if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case ADD:
                out << "ADD ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case MOD:
                out << "MOD ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case SUB:
                out << "SUB ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case SAVE:
                out << "SAVE ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case JGZ:
                out << "JGZ ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case MUL:
                out << "MUL ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case PAGE:
                out << "PAGE ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case READ:
                out << "READ ";
                if(commands[i].pointers_modifier)
                    out << "d";
				else
					out << "c";
                break;
            
            case SET:
                out << "SET ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;

            case WRITE:
                out << "WRITE ";
				if(commands[i].pointers_modifier)
					out<< "d";
				else
					out<< "c";
                break;

            case LOAD:
                out << "LOAD";
                break;

            case NOP:
                out << "NOP";
                break;

            case JMP:
                out << "JMP ";
                if(commands[i].pointers_modifier)
                    out << "&";
				out << commands[i].argument;
                break;
        }
		if(commands[i].commment != "")
			out<<" ;"<<commands[i].commment;
		out<<endl;
    }
	return;
}

void CreateCode::getAddress(string name, ExprAST *index){

    bool catched = false;
    if(in_function){

		addCommand(PAGE, false, stack_page, "");
		addCommand(SET, true, stack_pointer_address, "");
		addCommand(ADD, false, 12, "");

	

		int i;
		for(i=0; i<(int)t->child.size(); i++){
			if(name != t->child[i]->name)
				addCommand(ADD, false, 4, "");
			else{
				catched = true;
				break;
			}
		}

        if(catched){

			addCommand(LOAD, false, 0, "");
			addCommand(PAGE, false, buffer_page, "");
        }

    }

    if(!in_function || !catched){
		addCommand(PAGE, false, buffer_page, "");
		addCommand(SET, false, vars[name], "");
    }

    if(index){

		addCommand(SAVE, false, cur_address, "");
		cur_address+=4;
		index->accept(*this);

		addCommand(SET, true, cur_address, "");
		addCommand(MUL, false, 4, "");

		cur_address -= 4;

		//�����

		addCommand(ADD, true, cur_address, "");
    }

	addCommand(SAVE, false, var_buf, "");
	
	
	return;
}


void CreateCode::addCommand(Instruction name, bool pointers_modifier, int arg, string comment){
	Command command;
	command.name = name;
	command.pointers_modifier = pointers_modifier;
	command.argument = arg;
	command.commment = comment;
	commands.push_back(command);
	number_command ++;
	
}


void CreateCode::allocation(){

	int i;
	switch(t->name_struct){
		case VAR:
			vars[t->name] = cur_address;
			cur_address += 4;
			break;
		case ARRAY:
			vars[t->name] = cur_address;
			cur_address += (4*t->length);
			break;
		case PROG:
			for(i=0; i<(int)t->child.size(); i++){
				t = t->child[i];
				allocation();
				t = t->parent;
			}
			break;
		case CYCLE:
			for(i=0; i<(int)t->child.size(); i++){
				t = t->child[i];
				allocation();
				t = t->parent;
			}
			break;
		
		default: 
			break;
	}
	return;
}

vector<Command> CreateCode::getResalt(){
	return commands;
}

CreateCode::CreateCode(Table *t){
	this->t = t;
	
	buffer_page = 254;
	stack_page = 255;
	
	
	var_buf = 0;
	stack_pointer_address = 48;
	cur_address = 0;
	in_function = 0;
	number_command = 0;

	return;
}

void CreateCode::visit(ExprAST *e){ return; }

void CreateCode::visit(IntExprAST *e){
	
	addCommand(SET, false, e->getInt(), "");
	addCommand(SAVE, false, cur_address, "");
	
	return; 
}

void CreateCode::visit(RealExprAST *e){ return; }

void CreateCode::visit(IDExprAST *e){
	getAddress(e->getName(), NULL);

	addCommand(SET, true, var_buf, "");
	getVal(cur_address);//��������
	//addCommand(LOAD, false, 0, "");
	//addCommand(SAVE, false, cur_address, "");

	return;
}

void CreateCode::visit(VarExprAST *e){

	addCommand(NOP, false, 0, "var");
	
	
	if(contain(t, e->id->getName(), ARRAY, true) && !e->index){
		getAddress(e->id->getName(), NULL);
		//addCommand(SET, true, var_buf, "");
        addCommand(SAVE, false, cur_address, "");
	}
	else{
		getAddress(e->id->getName(), e->index);
		//addCommand(SET, true, var_buf, "");
		//addCommand(LOAD, false, 0, "");
        //addCommand(SAVE, false, cur_address, "");
		getVal(cur_address);
	}
	addCommand(NOP, false, 0, "var_end");	
	return; 
}

void CreateCode::visit(LabelExprAST *e){
	string name = e->id->getName();
	if(gotos.find(name) != gotos.end()){
		int i;
		for(i = 0; i<(int)gotos[name].size(); i++){
			commands[gotos[name][i]].argument = number_command;
		}
	}
	labels[name] = number_command;
        

	return;
}

void CreateCode::visit(GoToExprAST *e){
	string name = e->id->getName();
    if(labels.find(name) != labels.end()) // label is defined yet
		addCommand(JMP, false, labels[name], "");
    else{
        if(gotos.find(name) == gotos.end())
            gotos[name] = vector<int>();
        gotos[name].push_back(number_command);
		addCommand(JMP, false, 0, "");
    }

	return;
}

void CreateCode::visit(ReadExprAST *e){
	int i;
	
	for(i=0; i<(int)e->vars.size(); i++){
		VarExprAST *var = dynamic_cast<VarExprAST *>(e->vars[i]);
		getAddress(var->getName(), var->index);
		addCommand(READ, true, 0, "");
		//addCommand(SAVE, true, var_buf, "");
	
		addCommand(SAVE, false, cur_address + 4, "");

		addCommand(SET, true, var_buf, "");

		addCommand(DIV, false, 256 - 8, "");
		addCommand(SAVE, false, cur_address + 8, "");

		addCommand(SET, true, var_buf, "");
		addCommand(MOD, false, 256 - 8, "");
		addCommand(ADD, false, 8, "");
		addCommand(PAGE, true, cur_address + 8, "");

		addCommand(SAVE, false, 0, "");
		addCommand(PAGE, false, buffer_page, "");
		addCommand(SET, true, cur_address + 4, "");
		addCommand(PAGE, true, cur_address + 8, "");
		addCommand(SAVE, true, 0, "");

		addCommand(PAGE, false, buffer_page, "");
	}
	return;
}

void CreateCode::visit(OperandExprAST *e){
	e->operand->accept(*this);
	return;
}

void CreateCode::visit(BinaryExprAST *e){
	cur_address+=4;
	e->l_operand->accept(*this);
	cur_address+=4;
    e->r_operand->accept(*this);
	cur_address-=8;

    // ��������� �_�� � ���
	addCommand(SET, true, cur_address + 4, "");

    // ��������� �������
    if(e->op == "add")
		addCommand(ADD, true, cur_address + 8, "");

	if(e->op == "mul")
		addCommand(MUL, true, cur_address + 8, "");

    if(e->op == "min")
        addCommand(SUB, true, cur_address + 8, "");

    if(e->op == "div")
        addCommand(DIV, true, cur_address + 8, "");
  
    if(e->op == "mod")
        addCommand(MOD, true, cur_address + 8, "");

	//EQ, NE, LT, GT, LE, GE � �����, �� �����, ������, ������, ������ ��� �����, ������ ��� �����

	//>
	if(e->op == "gt"){
        addCommand(SUB, true, cur_address + 8, "");
		addCommand(JNZ, false, number_command + 3, "");
		addCommand(SET, false, 0, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 1, "");

    }

	//<=
    if(e->op == "le")
    {
		addCommand(SUB, true, cur_address + 8, "");
		addCommand(JNZ, false, number_command + 3, "");
		addCommand(SET, false, 1, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 0, "");

    }

	//<
    if(e->op == "lt")
    {
		addCommand(SET, true, cur_address + 8, "");
		addCommand(SUB, true, cur_address + 4, "");
		addCommand(JGZ, false, number_command + 3, "");
		addCommand(SET, false, 0, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 1, "");
    }

	//>=
    if(e->op == "ge")
    {
		addCommand(SET, true, cur_address + 8, "");
		addCommand(SUB, true, cur_address + 4, "");
		addCommand(JGZ, false, number_command + 3, "");
		addCommand(SET, false, 1, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 0, "");

    }
	//=
    if(e->op == "eq")
    {
        //add_command(SAVE, cur + 12, false); // a

		addCommand(SUB, true, cur_address + 8, "");
		addCommand(JGZ, false, number_command + 6, "");
		
		addCommand(SET, true, cur_address + 8, "");
		addCommand(SUB, true, cur_address + 4, "");
		addCommand(JGZ, false, number_command + 3, "");
		addCommand(SET, false, 1, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 0, "");
    }

	//!=
    if(e->op == "ne")
    {
        addCommand(SUB, true, cur_address + 8, "");
		addCommand(JGZ, false, number_command + 6, "");
		
		addCommand(SET, true, cur_address + 8, "");
		addCommand(SUB, true, cur_address + 4, "");
		addCommand(JGZ, false, number_command + 3, "");
		addCommand(SET, false, 0, "");
		addCommand(JMP, false, number_command + 2, "");
		addCommand(SET, false, 1, "");
    }

    // ���������
    addCommand(SAVE, false, cur_address, "");
	return; 
}

void CreateCode::visit(NegationExprAST *e){

	e->operand->accept(*this);

	//addCommand(SET, false, 0, "");
	getVal(cur_address);
	addCommand(SUB, true, cur_address, "");
	addCommand(SAVE, false, cur_address, "");

	return;
}

void CreateCode::visit(MovExprAST *e){

    VarExprAST *var = dynamic_cast<VarExprAST *>(e->var);

    e->expr->accept(*this); //��������� �������� ���������

	if(contain(t, var->id->getName(), ARRAY, true) && !var->index){
		vars[e->var->getName()] = vars[e->expr->getName()];
        
    }
    else{
        cur_address += 4;
		getAddress(var->getName(), var->index);

		addCommand(DIV, false, 256 - 8, "");
		addCommand(SAVE, false, cur_address + 4, "");

		addCommand(SET, true, var_buf, "");
		addCommand(MOD, false, 256 - 8, "");
		addCommand(ADD, false, 8, "");
		addCommand(PAGE, true, cur_address + 4, "");

		addCommand(SAVE, false, 0, "");
		addCommand(PAGE, false, buffer_page, "");
		addCommand(SET, true, cur_address - 4, "");
		addCommand(PAGE, true, cur_address + 4, "");
		addCommand(SAVE, true, 0, "");
		addCommand(PAGE, false, buffer_page, "");
    }
	return;
}

void CreateCode::visit(WriteExprAST *e){
	
	int i;
	for(i=0; i<(int)e->args.size(); i++){
		e->args[i]->accept(*this);
		addCommand(SET, true, cur_address, "");
		addCommand(WRITE, e->fl[i], 0, "");
	}
	return;
}

void CreateCode::visit(QualifierExprAST *e){
	if(e->name == "space")
		addCommand(SET, false, 32, "");
	if(e->name == "tab")
		addCommand(SET, false, 9, "");
	if(e->name == "skip")
		addCommand(SET, false, 13, "");

	addCommand(SAVE, false, cur_address, "");
            
	return;
}

void CreateCode::visit(BreakExprAST *e){
	breaks.push_back(number_command);
	addCommand(JMP, false, 0, ""); 
	return;
}

void CreateCode::visit(CallExprAST *e){

	addCommand(NOP, false, 0, "call " + e->name->getName());

	addCommand(PAGE, false, stack_page, "");
	cur_address = 4;
	
    if(in_function){

		addCommand(SET, true, stack_pointer_address, "");//�������� ����� �������� �����
		addCommand(SAVE, false, cur_address, "");//
		addCommand(ADD, false, 8, "");//�������� ����� ������ � ������ ����������
		addCommand(LOAD, false, 0, "");//�������� ����� ����������
		addCommand(ADD, true, cur_address, "");//��������� � ������ �������� ����� ����� ������ ��� ����������
		addCommand(ADD, false, 12, "");// ��������� ������ ��� ��������� � �������� ������ ������ �����
    }
    else{
		addCommand(SET, true, stack_pointer_address, "");
		addCommand(SAVE, false, cur_address, "");
    }
	//� �������� ������ ������ �����
	addCommand(SAVE, false, cur_address + 4, "");//��������� ������ ������ �����
	//addCommand(WRITE, true, 0, "");
	//addCommand(SAVE, false, stack_pointer_address, "");
	//addCommand(SET, true, stack_pointer_address, "");
	//addCommand(WRITE, true, 0, "");


	addCommand(ADD, false, 8, "");//�������� ����� ������ ��� �������� ����� ������, ����������� ��� ����������
	addCommand(SAVE, false, cur_address + 8, "");
	addCommand(SET, false, e->args.size()*4, "");//������� ����� ������ ��� ����������
	//addCommand(WRITE, true, 0, "");
	addCommand(SAVE, true, cur_address + 8, "");//��������� ����� �� ������������ ������

	addCommand(SET, true, cur_address + 8, "");//��������� ����� 3 ���� ������ �����
	addCommand(ADD, false, 4, "");//�������� ����� ��� 1 ����������
	addCommand(SAVE, false, cur_address + 8, "");//��������� ����� 1 ����������

    //int k = 0;

	addCommand(NOP, false, 0, "+ stack");

	cur_address += 12;

	int i;
	for(i=0; i<(int)e->args.size(); i++){
		
		VarExprAST *var = dynamic_cast<VarExprAST *>(e->args[i]);

		addCommand(PAGE, false, buffer_page, "");

        getAddress(var->getName(), var->index);//�������� ����� ����������
		addCommand(SET, true, var_buf, "");//��������� ���������� ����� � ���
		// addCommand(WRITE, true, 0, "");

		addCommand(PAGE, false, stack_page, "");

		addCommand(SAVE, true, cur_address - 4, "");//��������� ���������� ����� � ����

        addCommand(SET, true, cur_address - 4, "");//��������� ����� I ���������� � �����
		addCommand(ADD, false, 4, "");//�������� ������ I+1 ����������
		addCommand(SAVE, false, cur_address - 4, "");
        //k += 4;

	}
	cur_address -= 12;

	addCommand(SET, true, cur_address + 4, "");
	addCommand(SAVE, false, stack_pointer_address, "");//���������� ����� ��������� �� ����

	addCommand(NOP, false, 0, "end +");

    
    addCommand(SET, true, stack_pointer_address, "");//��������� ��������� �� ����
	addCommand(ADD, false, 4, "");//����� ��� �������� ��������� �� ���� ����
	addCommand(SAVE, false, cur_address + 8, "");


	addCommand(SET, true, cur_address, "");//��������� ����� ���� �����
	addCommand(SAVE, true, cur_address + 8, "");//��������� � ����

	addCommand(SET, true, stack_pointer_address, "");//������������� ����� �������� �����


	addCommand(SAVE, false, cur_address + 8, "");
	addCommand(SET, false, number_command + 4, "");
	addCommand(SAVE, true, cur_address + 8, "");
	

	

	addCommand(PAGE, false, buffer_page, "");
	//print_s(var_buf, 4);
	addCommand(JMP, false, functions[e->name->getName()], "");
	addCommand(NOP, false, 0, "end call");

	

	return;
}

void CreateCode::visit(TypeExprAST *e){ return; }

void CreateCode::visit(DescrExprAST *e){ return; }

void CreateCode::visit(IfExprAST *e){
	e->expr->accept(*this);
	int i, b_no, b_yes, e_no, e_yes;

	addCommand(SET, false, 0, "");
	addCommand(SAVE, false, cur_address + 4, "");

	addCommand(SUB, true, cur_address, "");
	addCommand(JGZ, false, number_command + 5, "");
		
	addCommand(SET, true, cur_address, "");
	addCommand(SUB, true, cur_address + 4, "");
	addCommand(JGZ, false, number_command + 2, "");

	b_no = number_command;
	for(i=0; i<(int)e->no.size(); i++){
		e->no[i]->accept(*this);
	}
	e_no = number_command;
	commands[number_command -(e_no - b_no) - 1].argument += (e_no - b_no);
	commands[number_command -(e_no - b_no) - 4].argument += (e_no - b_no);

	addCommand(JMP, false, number_command + 1, "");
	
	b_yes = number_command;
	for(i=0; i<(int)e->yes.size(); i++){
		e->yes[i]->accept(*this);
	}
	e_yes = number_command;

	commands[number_command -(e_yes - b_yes) -1].argument += (e_yes - b_yes);

	return;
}

void CreateCode::visit(CycleExprAST *e){
	int number = number_command;
	vector<int> buf = breaks;
	breaks.clear();
	int i;
	for(i=0; i<(int)e->body.size(); i++){
		e->body[i]->accept(*this);
	}
	addCommand(JMP, false, number, "");
	for(i = 0; i<(int)breaks.size(); i++){
		commands[breaks[i]].argument = number_command;
	}
	breaks.clear();
	breaks = buf;
	return;
}

void CreateCode::visit(ComponentExprAST *e){
	int i;
	for(i=0; i<(int)e->body.size(); i++){
		e->body[i]->accept(*this);
	}
	return;
}

void CreateCode::visit(OperatorExprAST *e){

	if(e->label){
		e->label->accept(*this);
	}
	if(e->unlabeled){
		e->unlabeled->accept(*this);
	}
	return;
}

void CreateCode::visit(FunctionAST *e){

	functions[e->id->getName()] = number_command;
	addCommand(NOP, false, 0, "function " + e->id->getName());
	in_function = 1;

	int i;
	for(i = 0; i<(int)t->child.size(); i++){
		if(e->id->getName() == t->child[i]->name){
			t = t->child[i];
			break;
		}
	}

	e->body->accept(*this);

	t = t->parent;

	in_function = 0;

	 

	addCommand(PAGE, false, stack_page, "");
	cur_address = 4;
	int cur = 4;
	   

	addCommand(SET, true, stack_pointer_address, "");//��������� ������ �������� �����
	//addCommand(WRITE, true, 0, "start I frame");
	addCommand(SAVE, false, cur, "");// ��������� ��� � ����� 
	addCommand(LOAD, false, 0, ""); // �������� ����� �����

	addCommand(SAVE, false, cur + 4, ""); //

    addCommand(SET, true, cur, ""); //���������� ����� ������ ������� �����
	addCommand(ADD, false, 4, ""); // �������� ����� ��������� �� ����. ����
	addCommand(LOAD, false, 0, ""); //����� ���� �����
	
	addCommand(SAVE, false, stack_pointer_address, ""); //��������� ����� �������� �����


	addCommand(SET, true, cur + 4, "");
	addCommand(PAGE, false, buffer_page, "");
	addCommand(SAVE, false, cur, "");
	addCommand(JMP, true, cur, "");

	return;
}

void CreateCode::visit(ProgramAST *e){
	allocation();
	cur_address = 4;

	addCommand(PAGE, false, stack_page, "");
	addCommand(SET, false, 52, "");
	addCommand(SAVE, false, stack_pointer_address, "");
	addCommand(PAGE, false, buffer_page, "");


	int i, b_fun;

	b_fun = number_command;
	addCommand(JMP, false, 0, "");
	for(i=0; i<(int)e->funs.size(); i++){
		e->funs[i]->accept(*this);
	}
	commands[b_fun].argument = number_command;

	addCommand(PAGE, false, buffer_page, "");
	e->body->accept(*this);

	return;
}





void CreateCode::getVal(int buf){

	addCommand(SAVE, false, buf, "");
	addCommand(DIV, false, 256 - 8, "");
	addCommand(SAVE, false, buf + 4, "");
	addCommand(SET, true, buf, "");
	addCommand(MOD, false, 256 - 8, "");
	addCommand(ADD, false, 8, "");
	addCommand(PAGE, true, buf + 4, "");
	addCommand(LOAD, false, 0, "");

	addCommand(PAGE, false, buffer_page, "");
	addCommand(SAVE, false, buf, "");
}

void CreateCode::print_s(int b, int n){
	addCommand(PAGE, false, stack_page, "");
	addCommand(SET, true, stack_pointer_address, "");
	addCommand(SAVE, false, b, "");
	int i;
	for(i=0; i<n; i++){
		addCommand(LOAD, false, 0, "");
		addCommand(WRITE, true, 0, "");
		addCommand(SET, true, b, "");
		addCommand(ADD, false, 4, "");
		addCommand(SAVE, false, b, "");
	}
	addCommand(PAGE, false, buffer_page, "");


}