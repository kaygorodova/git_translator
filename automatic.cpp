#include "automatic.h"

Automatic::Automatic(){
	return;
}

void Automatic::creationAutomatic(string name){
	this->name=name;
	initializationMatrix();
	creationMatrix(name);
	end=name.size()+1;
	return;
}
void Automatic::setName(string name){
	this->name=name;
	return;
}

void Automatic::initializationMatrix(){
	int i, j;
	for(i=0; i<MAX; i++)
		for(j=0; j<MAX; j++)
			matrix[i][j]=-1;
}

void Automatic::creationMatrix(string name){
	int i, j;

	for(i=0; i<(int)name.size(); i++){
		for(j=0; j<=(int)name.size(); j++){
			if(j==0)
				matrix[i][0]=name[i];

			if(j==i+1)
				matrix[i][j]=j+1;
		}
	}
	return;
}

void Automatic::creationAutomaticID(){
	initializationMatrix();
	name = "id";
	end=2;
	char q;
	int i, j;

	for(i=0, q='A'; q<='Z'; q++, i+=2){
		matrix[i][0]=q;
		matrix[i+1][0]=q+32;
		matrix[i][1]=2;
		matrix[i+1][1]=2;
	}


	for(i, q='A'; q<='Z'; q++, i+=2){
		matrix[i][0]=q;
		matrix[i+1][0]=q+32;
		matrix[i][2]=2;
		matrix[i+1][2]=2;
	}


	for(i, j='0'; j<='9'; j++, i++){
		matrix[i][0]=j;
		matrix[i][2]=2;
	}

	return;
}

void Automatic::creationAutomaticBinary(){
	initializationMatrix();
	name = "binary";
	end=4;
	char q;
	int i, k;

	matrix[0][0]='0';
	matrix[0][1]=2;

	for(q='B', i=1; i<3; i++, q+=32){
		matrix[i][0]=q;
		matrix[i][2]=3;
	}

	for(k=0; k<2; k++){
		for(q='0', i; q<='1'; i++, q++){
			matrix[i][0]=q;
			matrix[i][3+k]=4;
		}
	}
	return;
}

void Automatic::creationAutomaticOctal(){
	initializationMatrix();
	name = "octal";
	end=2;
	char q;
	int i;

	matrix[0][0]='0';
	matrix[0][1]=2;

	for(q='0', i=1; q<='7'; i++, q++){
		matrix[i][0]=q;
		matrix[i][2]=2;
	}

	return;
}

void Automatic::creationAutomaticDecimal(){
	initializationMatrix();
	name = "decimal";
	end=2;
	char q;
	int i;

	for(q='1', i=0; q<='9'; i++, q++){
		matrix[i][0]=q;
		matrix[i][1]=2;
	}

	for(q='0', i; q<='9'; i++, q++){
		matrix[i][0]=q;
		matrix[i][2]=2;
	}

	return;
}

void Automatic::creationAutomaticHexadecimal(){
	initializationMatrix();
	name = "hexadecimal";
	end=4;
	char q;
	int i, j;

	matrix[0][0]='0';
	matrix[0][1]=2;

	for(q='X', i=1; i<3; i++, q+=32){
		matrix[i][0]=q;
		matrix[i][2]=3;
	}
	for(j=0; j<2; j++){

		for(q='0', i; q<='9'; i++, q++){
			matrix[i][0]=q;
			matrix[i][3+j]=4;
		}

	
		for(q='A', i; q<='F'; i+=2, q++){
			matrix[i][0]=q;
			matrix[i+1][0]=q+32;
			matrix[i][3+j]=4;
			matrix[i+1][3+j]=4;
		}
	}
	return;
}

void Automatic::creationAutomaticReal(){
	initializationMatrix();

	name = "real";
	end=7;
	
	int i, k;
	char q;

	for(i=0, q='0'; q<='9'; i++, q++){
		matrix[i][0]=q;
		matrix[i][1]=2;
	}

	matrix[i][0]='.';
	matrix[i][2]=3;
	i++;

	for(k=0; k<2; k++){
		for(i, q='0'; q<='9'; q++, i++){
			matrix[i][0]=q;
			matrix[i][3+k]=4;
		}
	}

	q ='E';
	for(k=0; k<2; k++){
		matrix[i][0]=q;
		matrix[i][4]=5;
		i++;
		q+=32;
	}

	matrix[i][0]='+';
	matrix[i][5]=6;
	i++;

	matrix[i][0]='-';
	matrix[i][5]=6;
	i++;

	for(k=0; k<3; k++){
		for(i, q='0'; q<='9'; q++, i++){
			matrix[i][0]=q;
			matrix[i][5+k]=7;
		}
	}

	
	return;
}

int Automatic::simulationOfTheAutomaton(string str){
	pos=1;
	int i=0, j=0;
	while(i!=MAX){
		for(i=0; i<MAX; i++)
			if(j<str.size() && matrix[i][0]==(int)str[j] && matrix[i][pos]!=-1){
				pos=matrix[i][pos];
				j++;
				break;
			}
	}

	if(pos==end)
		return j;
	return 0;

}

string Automatic::getName(){
	return name;
}

Automatic::~Automatic(){
	return;
}