#include <stdlib.h>
#include <math.h>
#include "lexer.h"

vector<Lexema> getAllLexemas(ifstream &in){
	string str;
	int i=1;
	vector<Lexema> m;

	while (getline(in, str)){
		scanner(str, i, m);
		i++;
	}
	return m;
}

bool validityOfTokens(vector<Lexema> m){
	int i, numberOfErrors=0;
	for(i=0; i<(int)m.size(); i++){
		if(m[i].lexema=="Error"){
			cout<<"Error:"<<m[i].line_number<<":"<<m[i].value<<endl;
			numberOfErrors++;
		}
	}
	if(numberOfErrors)
		return false;
	return true;
}

bool printLexem(vector<Lexema> m, ostream &out){


	int i, numberOfErrors=0;
	for(i=0; i<(int)m.size(); i++){
		if(m[i].lexema=="Error"){
			cout<<"Error:"<<m[i].line_number<<":"<<m[i].value<<endl;
			numberOfErrors++;
		}

		out<<m[i].line_number<<"\tlex:"<<m[i].lexema<<"\t";
		if(m[i].type=="int")
			out<<m[i].type<<":"<<m[i].recognized_value_int<<"\t";
		if(m[i].type=="real")
			out<<m[i].type<<":"<<m[i].recognized_value_real<<"\t";
		out<<"val:"<<m[i].value<<endl;
		
	}
	if(numberOfErrors==0)
		return true;

	return false;
}

vector<Automatic> setAll(){
	vector<Automatic> m;

	Automatic a;

	a.creationAutomaticBinary();
	m.push_back(a);

	a.creationAutomaticDecimal();
	m.push_back(a);

	a.creationAutomaticHexadecimal();
	m.push_back(a);

	a.creationAutomaticOctal();
	m.push_back(a);

	a.creationAutomaticReal();
	m.push_back(a);

	a.creationAutomatic("add");
	m.push_back(a);

	a.creationAutomatic("mul");
	m.push_back(a);

	a.creationAutomatic("skip");
	m.push_back(a);

	a.creationAutomatic("space");
	m.push_back(a);

	a.creationAutomatic("tab");
	m.push_back(a);

	a.creationAutomatic("div");
	m.push_back(a);

	a.creationAutomatic("mod");
	m.push_back(a);

	a.creationAutomatic("sub");
	a.setName("min");
	m.push_back(a);

	a.creationAutomatic("equ");
	a.setName("eq");
	m.push_back(a);

	a.creationAutomatic("neq");
	a.setName("ne");
	m.push_back(a);

	a.creationAutomatic("lth");
	a.setName("lt");
	m.push_back(a);

	a.creationAutomatic("gth");
	a.setName("gt");
	m.push_back(a);

	a.creationAutomatic("leq");
	a.setName("le");
	m.push_back(a);

	a.creationAutomatic("geq");
	a.setName("ge");
	m.push_back(a);

	a.creationAutomatic("mov");
	a.setName("let");
	m.push_back(a);

	a.creationAutomatic("box");
	a.setName("beg");
	m.push_back(a);

	a.creationAutomatic("end");
	m.push_back(a);

	a.creationAutomatic("(");
	a.setName("lrb");
	m.push_back(a);

	a.creationAutomatic(")");
	a.setName("rrb");
	m.push_back(a);

	a.creationAutomatic("[");
	a.setName("lsb");
	m.push_back(a);

	a.creationAutomatic("]");
	a.setName("rsb");
	m.push_back(a);

	a.creationAutomatic(":");
	a.setName("colon");
	m.push_back(a);

	a.creationAutomatic(",");
	a.setName("comma");
	m.push_back(a);

	a.creationAutomatic(";");
	a.setName("semicolon");
	m.push_back(a);

	a.creationAutomatic("var");
	m.push_back(a);

	a.creationAutomatic("int");
	a.setName("typeint");
	m.push_back(a);

	a.creationAutomatic("real");
	a.setName("typereal");
	m.push_back(a);

	a.creationAutomatic("goto");
	m.push_back(a);

	a.creationAutomatic("break");
	m.push_back(a);

	a.creationAutomatic("if");
	m.push_back(a);

	a.creationAutomatic("then");
	m.push_back(a);

	a.creationAutomatic("else");
	m.push_back(a);

	a.creationAutomatic("loop");
	m.push_back(a);

	a.creationAutomatic("procedure");
	m.push_back(a);

	a.creationAutomatic("implementation");
	m.push_back(a);

	a.creationAutomatic("read");
	m.push_back(a);

	a.creationAutomatic("write");
	m.push_back(a);

	a.creationAutomaticID();
	m.push_back(a);

	return m;
}

int translationOfInt(string str, int base_system){
	int res, k, i;
	k=1;
	res=0;
	long long resl;
	resl=0;
	for(i=str.size()-1; i>=0; i--){
		if(str[i]>='0' && str[i]<='9'){
			res+=(str[i]-'0')*k;
			resl+=(str[i]-'0')*k;
		}
		else{
			if(str[i]>='a' && str[i]<='f'){
				res+=(str[i]-'a')*k;
				resl+=(str[i]-'a')*k;
			}
			else{
				res+=(str[i]-'A'+10)*k;
				resl+=(str[i]-'A'+10)*k;
			}
		}
		if((long long)res!=resl || k==0)
			return -1;
		k*=base_system;

	}
	return res;

}

void error(string val, vector<Lexema> &m, int line_number){
	Lexema l;
	l.line_number=line_number;
	l.lexema="Error";
	l.value=val;
	m.push_back(l);
}

void scanner(string str, int line_number, vector<Lexema> &m){
	vector<Automatic> a;
	a=setAll();
	int i;

	while((int)str.size()!=0){

		for(i=0; i<(int)str.size(); i++){
			if(!isspace(str[i]))
				break;
		}

		if(i!=str.size())
			str=str.substr(i, str.size()-i);
		else
			return;

		if(((int)str.size()>3 && isspace(str[3]) || (int)str.size()==3) && str.substr(0,3)=="rem")
				return;
		
		int max=0, k=-1, res;
		for(i=0; i<(int)a.size(); i++){
			res=a[i].simulationOfTheAutomaton(str);
			if(res>max){
				max=res;
				k=i;
			}
		}

		if(k==-1){
			error("Incorrect value", m, line_number);
			return;
		}

		string name=a[k].getName();
		Lexema l;
		l.line_number=line_number;


		if(name=="binary" || name=="octal" || name=="decimal"|| name=="hexadecimal"){
			
			if(name=="binary"){
				l.recognized_value_int=translationOfInt(str.substr(2,max), 2);
			}

			if(name=="octal"){
				l.recognized_value_int=translationOfInt(str.substr(0,max), 8);
			}

			if(name=="decimal"){
				l.recognized_value_int=translationOfInt(str.substr(0,max), 10);
			}

			if(name=="hexadecimal"){
				l.recognized_value_int=translationOfInt(str.substr(2,max), 16);
			}

			if(l.recognized_value_int==-1){
				error("Overflow", m, line_number);
				return;
			}

			l.lexema="int";
			l.type="int";

		}
		else{
			l.lexema=name;

			if(name=="real"){
				char *end_ptr;
				end_ptr=new char;
				l.recognized_value_real=strtod(str.substr(0,max).c_str(), &end_ptr);
				if(l.recognized_value_real==HUGE_VAL || l.recognized_value_real==-HUGE_VAL){
					error("Overflow", m, line_number);
					return;
				}
				l.type="real";
			}
		}

		l.value=str.substr(0,max);

		m.push_back(l);

		if(max!=str.size())
			str=str.substr(max, str.size()-max);
		else
			str="";

		if((l.lexema=="int" || l.lexema=="real") && str[0]!=' '){
			string invalid_characters="qwertyuiopasdfghjklzxcvbnmQWERTYUIOASDFGHJKLZXCVBNM.1234567890";
			for(i=0; i<(int)invalid_characters.size(); i++){
				if(invalid_characters[i]==str[0]){
					error("Incorrect symbol after number", m, line_number);
					return;
				}
			}
		}

		
	}
	return;
}




