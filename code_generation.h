#ifndef code_generation_h
#define code_generation_h
#include <vector>
#include <string>
#include <map>
#include <iostream>
#include "grammar.h"


using namespace std;

struct Table;
class Visitor;

enum Instruction{
	ADD,
	DIV,
	JGZ,
	JMP,
	JNZ,
	LOAD,
	MOD,
	MUL,
	NOP,
	PAGE,
	READ,
	SAVE,
	SET,
	SUB,
	WRITE
};


struct Command{
	Instruction name;
	bool pointers_modifier;
	int argument;
	string commment;
};






class CreateCode : public Visitor{

	map<string, int> vars;
	Table *t;
	vector<Command> commands;
	
	map<string, int> labels;
	map<string, vector<int> > gotos;
	map<string, int> functions;
	vector<int> breaks;
	int number_command;
	int stack_pointer_address;
	int cur_address;
    int in_function;
	int var_buf;

	int buffer_page;
	int stack_page;

	void allocation();
	void addCommand(Instruction name, bool pointers_modifier, int arg, string comment);
	void getAddress(string name, ExprAST *index);
	void getVal(int buf);
	void print_s(int b, int n);

public:
	vector<Command> getResalt();
	void printCode(ostream &out);
	CreateCode(Table *t);

	void visit(ExprAST *e);
	void visit(IntExprAST *e);
	void visit(RealExprAST *e);
	void visit(IDExprAST *e);
	void visit(VarExprAST *e);
	void visit(LabelExprAST *e);
	void visit(GoToExprAST *e);
	void visit(ReadExprAST *e);
	void visit(OperandExprAST *e);
	void visit(BinaryExprAST *e);
	void visit(NegationExprAST *e);
	void visit(MovExprAST *e);
	void visit(WriteExprAST *e);
	void visit(QualifierExprAST *e);
	void visit(BreakExprAST *e);
	void visit(CallExprAST *e);
	void visit(TypeExprAST *e);
	void visit(DescrExprAST *e);
	void visit(IfExprAST *e);
	void visit(CycleExprAST *e);
	void visit(ComponentExprAST *e);
	void visit(OperatorExprAST *e);
	void visit(FunctionAST *e);
	void visit(ProgramAST *e);


};




#endif