#include "grammar.h"
#include "code_generation.h"
#include <map>
using namespace std;

//������


void ExprAST::printAttrib(ostream &out){

	return;
}

void ExprAST::printTag(ostream &out){
	return;
}


void IntExprAST::printTag(ostream &out){

	out<<"<int val=\""<<val<<"\"/>"<<endl;
	return;
	
}

void IntExprAST::printAttrib(ostream &out){
	out<<"\""<<val<<"\"";
	return;
}

void RealExprAST::printTag(ostream &out){
	out<<"<real val=\""<<val<<"\"/>"<<endl;
	return;
}

void IDExprAST::printAttrib(ostream &out){
	out<<"\""<<name<<"\"";
	return;
}

void VarExprAST::printTag(ostream &out){
	out<<"<var name=";
	id->printAttrib(out);
	if(index!=NULL){
		out<<" index=";
		index->printAttrib(out);
	}
	out<<"/>"<<endl;
	return;
}

void LabelExprAST::printTag(ostream &out){
	out<<"<label name=";
	id->printAttrib(out);
	out<<"/>"<<endl;
	return;
}

void GoToExprAST::printTag(ostream &out){
	out<<"<goto label=";
	id->printAttrib(out);
	out<<"/>"<<endl;
	return;
}

void ReadExprAST::printTag(ostream &out){
	out<<"<read>"<<endl;
	int i;
	for(i=0; i<(int)vars.size(); i++){
		vars[i]->printTag(out);
	}
	out<<"</read>"<<endl;
	return;
}

void OperandExprAST::printTag(ostream &out){
	out<<"<expr>"<<endl<<"<op>"<<endl;
	operand->printTag(out);
	out<<"</op>"<<endl<<"</expr>"<<endl;
	return;
}

void BinaryExprAST::printTag(ostream &out){
	out<<"<expr>"<<endl<<"<op kind=\""<<op<<"\">"<<endl;
	r_operand->printTag(out);
	l_operand->printTag(out);
	out<<"</op>"<<endl<<"</expr>"<<endl;
	return;
}

void NegationExprAST::printTag(ostream &out){
	out<<"<expr>"<<endl<<"<op kind=\"min\">"<<endl;
	operand->printTag(out);
	out<<"</op>"<<endl<<"</expr>"<<endl;
	return;
}

void MovExprAST::printTag(ostream &out){
	out<<"<assign>"<<endl;
	var->printTag(out);
	expr->printTag(out);
	out<<"</assign>"<<endl;
	return;
}

void WriteExprAST::printTag(ostream &out){
	out<<"<write>"<<endl;
	int i;
	for(i=0; i<(int)args.size(); i++){
		args[i]->printTag(out);
	}
	out<<"</write>"<<endl;
	return;
}


void QualifierExprAST::printTag(ostream &out){
	out<<"<qualifier kind=\""<<name<<"\"/>"<<endl;
	return;
}

void BreakExprAST::printTag(ostream &out){
	out<<"<break/>"<<endl;
	return;
}

void CallExprAST::printTag(ostream &out){
	out<<"<call name=";
	name->printAttrib(out);
	out<<">"<<endl;
	int i;
	for(i=0; i<(int)args.size(); i++){
		args[i]->printTag(out);
	}
	out<<"</call>"<<endl;
	
	return;
}


void TypeExprAST::printAttrib(ostream &out){
	out<<"\""<<name<<"\"";
	return;
}

void DescrExprAST::printTag(ostream &out){
	int i;
	for(i=0; i<(int)args.size(); i++){
		out<<"<dfn name=";
		args[i].first->printAttrib(out);
		
		if(args[i].second!=NULL){
			out<<" length=";
			args[i].second->printAttrib(out);
		}
		out<<" type=";
		type->printAttrib(out);
		out<<"/>"<<endl;
	}

	
	return;
}
void IfExprAST::printTag(ostream &out){
	int i;
	out<<"<if>"<<endl;
	expr->printTag(out);
	out<<"<then>"<<endl;
	for(i=0; i<(int)yes.size(); i++){
		yes[i]->printTag(out);
	}
	out<<"</then>"<<endl;
	out<<"<else>"<<endl;
	for(i=0; i<(int)no.size(); i++){
		no[i]->printTag(out);
	}
	out<<"</else>"<<endl;
	out<<"</if>"<<endl;
	return;
}

void CycleExprAST::printTag(ostream &out){
	int i;
	out<<"<while>"<<endl;
	for(i=0; i<(int)body.size(); i++){
		body[i]->printTag(out);
	}
	out<<"</while>"<<endl;
	return;
}

void ComponentExprAST::printTag(ostream &out){
	int i;
	out<<"<compound>"<<endl;
	for(i=0; i<(int)body.size(); i++){
		body[i]->printTag(out);
	}
	out<<"</compound>"<<endl;
	return;
}


void OperatorExprAST::printTag(ostream &out){
	out<<"<clause>"<<endl;
	if(label!=NULL)
		label->printTag(out);
	if(unlabeled!=NULL)
		unlabeled->printTag(out);	
	out<<"</clause>"<<endl;
	return;
}

void FunctionAST::printTag(ostream &out){
	out<<"<procedure name=";
	id->printAttrib(out);
	out<<">"<<endl;
	int i;
	for(i=0; i<(int)args.size(); i++){
		args[i]->printTag(out);
	}
	out<<"</procedure>"<<endl;
	
	return;
}

void ProgramAST::printTag(ostream &out){
	out<<"<program>"<<endl;
	int i;
	for(i=0; i<(int)vars.size(); i++){
		vars[i]->printTag(out);
	}

	for(i=0; i<(int)funs.size(); i++){
		funs[i]->printTag(out);
	}

	body->printTag(out);

	out<<"</program>"<<endl;

	return;
}




//������� ����
int ExprAST::getInt(){
	return -1;
}

int IntExprAST::getInt(){
	return val;
}

TypeNumber ExprAST::getTypeNumber(Table *t){
	return NO;
}


TypeNumber TypeExprAST::getTypeNumber(Table *t){
	if(name=="int")
		return INT;
	return REAL;
}

TypeNumber IntExprAST::getTypeNumber(Table *t){
	return INT;
}

TypeNumber RealExprAST::getTypeNumber(Table *t){
	return REAL;
}

TypeNumber IDExprAST::getTypeNumber(Table *t){
	return NO;
}

TypeNumber VarExprAST::getTypeNumber(Table *t){
	Table *l;
	l = contain(t, id->getName(), true);
	return l->type;
}

TypeNumber OperandExprAST::getTypeNumber(Table *t){
	return operand->getTypeNumber(t);
}

TypeNumber BinaryExprAST::getTypeNumber(Table *t){
	TypeNumber l, r;
	l = l_operand->getTypeNumber(t);
	r = r_operand->getTypeNumber(t);

	if(op == "eq" || op == "ne" || op == "lt" || op == "gt" || op == "le" || op == "ge")
		return INT;

	if(l == REAL || r == REAL){
		if(op == "mod")
			throwException(l_operand->getNumberLine(), "Operation is not defined on real");
		return REAL;
	}

	return INT;
}

TypeNumber NegationExprAST::getTypeNumber(Table *t){
	return operand->getTypeNumber(t);
}



TypeStruct ExprAST::getTypeStruct(Table *t){
	return OTHER;
}

TypeStruct IntExprAST::getTypeStruct(Table *t){
	return VAR;
}

TypeStruct RealExprAST::getTypeStruct(Table *t){
	return VAR;
}

TypeStruct OperandExprAST::getTypeStruct(Table *t){
	return operand->getTypeStruct(t);
}

TypeStruct BinaryExprAST::getTypeStruct(Table *t){
	if(l_operand->getTypeStruct(t) == ARRAY){
		throwException(l_operand->getNumberLine(), "Operation is not defined on arrays");
		return OTHER;
	}
	if(r_operand->getTypeStruct(t) == ARRAY){
		throwException(r_operand->getNumberLine(), "Operation is not defined on arrays");
		return OTHER;
	}
	return VAR;
}

TypeStruct NegationExprAST::getTypeStruct(Table *t){
	if(operand->getTypeStruct(t) == ARRAY){
		throwException(operand->getNumberLine(), "Operation is not defined on arrays");
		return OTHER;
	}
	return VAR;
}

TypeStruct VarExprAST::getTypeStruct(Table *t){
	Table *l;
	l = contain(t, id->getName(), true);
	if(l->name_struct == ARRAY && !index)
		return ARRAY;
	return VAR;
}

string ExprAST::getName(){
	return string("");
}

string IDExprAST::getName(){
	return name;
}

string VarExprAST::getName(){
	return id->getName();
}

string OperandExprAST::getName(){
	return operand->getName();
}


void ExprAST::createTable(Table *t){
	return;
}

void VarExprAST::createTable(Table *t){
	Table *l;
	l = contain(t, id->getName(), true);
	if(!l){
		throwException(id->getNumberLine(), id->getName() + " undeclared identifier");
	}
	l->ref++;
	
	if(l->name_struct == VAR ){
		if(!index)
			return;
		throwException(id->getNumberLine(), "Index requires array");
	}

	if(l->name_struct == ARRAY){
		if(!index || index->getTypeNumber(t) == INT)
			return;

		if(index->getTypeNumber(t) == REAL)
			throwException(index->getNumberLine(), "Index is real");

		l = contain(t, index->getName(), true);

		if(!l){
			throwException(index->getNumberLine(), index->getName() + " undeclared identifier");
		}

		l->ref++;
		if(l->name_struct == VAR && l->type == INT)
			return;

		throwException(index->getNumberLine(), "Incorrect index");

	}

	throwException(id->getNumberLine(), "Required variable");

}

void LabelExprAST::createTable(Table *t){
	Table *l;
	l = contain(t, id->getName(), true);
	if(!l){
		l = new Table();
		l->fl = true;
		l->name = id->getName();
		l->name_struct = LABEL;
		l->parent = t;
		l->ref = 1;
		l->type = NO;
		
		t->child.push_back(l);
		return;
	}

	if(l->name_struct == LABEL){
		if(l->fl)
			throwException(id->getNumberLine(), "Redefinition " + id->getName());
		l->ref++;
		l->fl = true;
		return;
	}
	throwException(id->getNumberLine(), "Redefinition " + id->getName());
	return;

}

void GoToExprAST::createTable(Table *t){
	Table *l;
	l = contain(t, id->getName(), true);
	if(!l){
		l = new Table();
		l->fl = false;
		l->name = id->getName();
		l->name_struct = LABEL;
		l->parent = t;
		l->ref = 1;
		l->type = NO;
		l->number_line = id->getNumberLine();
		
		t->child.push_back(l);
		return;
	}

	if(l->name_struct == LABEL){
		l->ref++;
		return;
	}

	throwException(id->getNumberLine(), "Redefinition " + id->getName());
	return;

}

void ReadExprAST::createTable(Table *t){
	int i;
	for(i=0; i<(int)vars.size(); i++){
		vars[i]->createTable(t);
		vars[i]->getTypeNumber(t);
		if(vars[i]->getTypeStruct(t) == ARRAY)
			throwException(vars[i]->getNumberLine(), "Required intex");
	}
	return;
}

void OperandExprAST::createTable(Table *t){
	operand->createTable(t);
	operand->getTypeNumber(t);
	operand->getTypeStruct(t);
	return;
}

void BinaryExprAST::createTable(Table *t){
	l_operand->createTable(t);
	r_operand->createTable(t);
	return;
}

void NegationExprAST::createTable(Table *t){
	operand->createTable(t);
}

void MovExprAST::createTable(Table *t){
	var->createTable(t);
	expr->createTable(t);
	if(var->getTypeNumber(t) == INT && expr->getTypeNumber(t) == REAL)
		throwException(number_line, "Compatible assignment");
	
	TypeStruct type_var, type_expr;

	type_var = var->getTypeStruct(t);
	type_expr = expr->getTypeStruct(t);

	if(type_var != type_expr)
		throwException(number_line, "Compatible assignment");

	if(type_var == ARRAY){
		if(contain(t, var->getName(), true)->length != contain(t, expr->getName(), true)->length)
			throwException(number_line, "Compatible assignment");
	}
	return;

}

void WriteExprAST::createTable(Table *t){
	int i;
	for(i=0; i<(int)args.size(); i++){
		if(args[i]->getName() != "" && contain(t, args[i]->getName(), true)->name_struct == ARRAY){
			OperandExprAST *op = dynamic_cast<OperandExprAST *>(args[i]);
			VarExprAST *var = dynamic_cast<VarExprAST *>(op->operand);
			if(!var->index)
				throwException(number_line, "Required intex");
		}
		args[i]->createTable(t);
		args[i]->getTypeNumber(t);
		args[i]->getTypeStruct(t);
	}
	return;
}

void DescrExprAST::createTable(Table *t){
	
	int i;
	Table *l;

	for(i=0; i<(int)args.size(); i++){
		l = contain(t, args[i].first->getName(), true);
		
		if(!l || ((l->name_struct == ARRAY || l->name_struct == VAR) && l->parent != t)){
		
			l = new Table();
			l->name = args[i].first->getName();
			l->ref = 1;
			l->type = type->getTypeNumber(t);
			l->parent = t;
				
			if(args[i].second){
				l->length = args[i].second->getInt();
				l->name_struct = ARRAY;
			}
			else{
				l->name_struct = VAR;
			}
			t->child.push_back(l);			
			continue;

		}
		throwException(args[i].first->getNumberLine(), "Redefinition " + args[i].first->getName());

	}


	return;
}

void IfExprAST::createTable(Table *t){
	expr->createTable(t);
	TypeStruct t_s = expr->getTypeStruct(t);
	TypeNumber t_n = expr->getTypeNumber(t);

	if(t_s != VAR || t_n != INT)
		throwException(expr->getNumberLine(), "Incorrect expression in condition");

	int i;
	for(i=0; i<(int)yes.size(); i++){
		yes[i]->createTable(t);
	}
	for(i=0; i<(int)no.size(); i++){
		no[i]->createTable(t);
	}
	return;
}
void BreakExprAST::createTable(Table *t){
	if(t->name_struct != CYCLE)
		throwException(number_line, "Break is not in the loop");

}

void CycleExprAST::createTable(Table *t){
	Table *l;
	l = new Table();
	l->name_struct = CYCLE;
	l->parent = t;
	t->child.push_back(l);
	
	int i;
	for(i=0; i<(int)body.size(); i++){
		body[i]->createTable(l);
	}
	return;
}
void ComponentExprAST::createTable(Table *t){
	int i;
	for(i=0; i<(int)body.size(); i++){
		body[i]->createTable(t);
	}
	return;
}

void OperatorExprAST::createTable(Table *t){
	if(label)
		label->createTable(t);
	if(unlabeled)
		unlabeled->createTable(t);
	return;
}

void CallExprAST::createTable(Table *t){
	Table *l;
	l = contain(t, name->getName(), FUN, true);
	if(!l)
		throwException(number_line, name->getName() + " undeclared function");
	if(l->child.size()< args.size() || (l->child.size() != args.size() && (l->child[args.size()]->name_struct == VAR || l->child[args.size()]->name_struct == ARRAY)))
		throwException(number_line, "Incorrect number of arguments");
	l->ref++;

	int i;
	for(i=0; i<(int)args.size(); i++){
		args[i]->createTable(t);

		if(l->child[i]->type == INT && args[i]->getTypeNumber(t) == REAL)
			throwException(args[i]->getNumberLine(), "Compatible assignment");

		TypeStruct type_var = args[i]->getTypeStruct(t);

		if(type_var != l->child[i]->name_struct)
			throwException(args[i]->getNumberLine(), "Compatible assignment");

		if(type_var == ARRAY){
			if(l->child[i]->length != contain(t, args[i]->getName(), true)->length)
				throwException(args[i]->getNumberLine(), "Compatible assignment");
		}
	}

	return;
}

void FunctionAST::createTable(Table *t){
	Table *l;
	l = contain(t, id->getName(), false);
	if(!l){
		l = new Table();
		l->name = id->getName();
		l->name_struct = FUN;
		l->ref = 1;
		l->type = NO;
		l->parent = t;
	
		t->child.push_back(l);
	}
	else
		throwException(id->getNumberLine(), "Redefinition " + id->getName());

	int i;
	for(i=0; i<(int)args.size(); i++){
		args[i]->createTable(l);
	}

	body->createTable(l);
	return;
}

void ProgramAST::createTable(Table *t){
	int i;
	for(i=0; i<(int)vars.size(); i++){
		vars[i]->createTable(t);
	}

	for(i=0; i<(int)funs.size(); i++){
		funs[i]->createTable(t);
	}
	
	body->createTable(t);

	return;
}

void ExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void IntExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void RealExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}
void IDExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}
void VarExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void LabelExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}
void GoToExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void ReadExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}
void OperandExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}
void BinaryExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void NegationExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void MovExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void WriteExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void QualifierExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void BreakExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void CallExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void TypeExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void DescrExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void IfExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void CycleExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void ComponentExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void OperatorExprAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void FunctionAST::accept(Visitor &v){
	v.visit(this);
	return;
}

void ProgramAST::accept(Visitor &v){
	v.visit(this);
	return;
}

	








