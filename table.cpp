#include "table.h"
#include "grammar.h"



Table* contain(Table *t, string name, TypeStruct type, bool check_parent){

	if(!t)
		return NULL;

	while(true){
		int i;
		for(i=0; i<(int)t->child.size(); i++){
			if(t->child[i]->name == name && t->child[i]->name_struct == type){
				return t->child[i];
			}	
		}
		
		if(t->name_struct == CYCLE){
			t = t->parent;
			continue;
		}

		if(!check_parent)
			break;

		t = t->parent;

		if(!t)
			break;
	}

	return NULL;
}

Table* contain(Table *t, string name, bool check_parent){

	if(!t)
		return NULL;

	while(true){
		int i;
		for(i=0; i<(int)t->child.size(); i++){
			if(t->child[i]->name == name){
				return t->child[i];
			}	
		}
		if(t->name_struct == CYCLE){
			t = t->parent;
			continue;
		}

		if(!check_parent)
			break;

		t = t->parent;

		if(!t)
			break;
	}

	return NULL;

}

Table* getTable(ExprAST* tree){
	Table *table;
	table = new Table;
	table->name_struct = PROG;
	table->parent = NULL;
	table->type = NO;

	try{
		tree->createTable(table);
		volidTable(table);
	}
	catch(string e){
		cout<<"Error:"+e;
		return NULL;
	}	

	return table;

}

void volidTable(Table *t){

	int i;
	switch(t->name_struct){
		case FUN:
			for(i=0; i<(int)t->child.size(); i++){
				volidTable(t->child[i]);
			}
			break;
		case LABEL:
			if(!t->fl)
				throwException(t->number_line, t->name + " undeclared label");
			break;
		case PROG:
			for(i=0; i<(int)t->child.size(); i++){
				volidTable(t->child[i]);
			}
			break;
		case CYCLE:
			for(i=0; i<(int)t->child.size(); i++){
				volidTable(t->child[i]);
			}
			break;
		
		default: 
			break;

	}
	return;
}

bool printTable(int num, Table *t, ostream &out){
	int i;
	if(t==NULL){
		if(num!=0)
			return false;
		out<<"<?xml version=\"1.0\" ?>"<<endl<<"<table>"<<endl<<"</table>"<<endl;
		return true;
	}

	switch(t->name_struct){
		case VAR:
			out<<"<entry name=\""<<t->name<<"\" type=\""<<printType(t->type)<<"\" ref=\""<<t->ref<<"\"/>"<<endl;
			break;
		case ARRAY:
			out<<"<entry name=\""<<t->name<<"\" length=\""<<t->length<<"\" type=\""<<printType(t->type)<<"\" ref=\""<<t->ref<<"\"/>"<<endl;
			break;
		case FUN:
			out<<"<entry name=\""<<t->name<<"\" type=\"procedure\" ref=\""<<t->ref<<"\">"<<endl;
			for(i=0; i<(int)t->child.size(); i++){
				printTable(num, t->child[i], out);
			}
			out<<"</entry>"<<endl; 
			break;
		case LABEL:
			out<<"<entry name=\""<<t->name<<"\" type=\"label\" ref=\""<<t->ref<<"\"/>"<<endl;
			break;
		case PROG:
			out<<"<?xml version=\"1.0\" ?>"<<endl<<"<table>"<<endl;
			for(i=0; i<(int)t->child.size(); i++){
				printTable(num, t->child[i], out);
			}
			out<<"</table>"<<endl;
			break;
		case CYCLE:
			for(i=0; i<(int)t->child.size(); i++){
				printTable(num, t->child[i], out);
			}
		default: 
			break;

	}
	return true;

}

string printType(TypeNumber type){
	if(type == INT)
		return "int";
	return "real";
}

bool validityOfTable(int num, Table *t){
	if(t==NULL){
		if(num!=0)
			return false;
		return true;
	}
	return true;
}
