#include "parser.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

vector<Lexema> m;
int curTok;

bool setToken(vector<Lexema> v){
	m=v;
	if(m.size()==0)
		return false;
	curTok=0;
	return true;
}
void getNextToken(){
	curTok++;
	return;
}

void recoil(int i) {
	curTok = i;
	return;
}

int getNumberToken(){
	return curTok;
}

void expect(string str){
	//char t[30];
	if(curTok<(int)m.size()){
		if(m[curTok].lexema==str){
			getNextToken();			
			return;
		}
		throwException(m[curTok].line_number, "Needs another token instead " + m[curTok].value);
		//itoa(m[curTok].line_number,t,10);
		//throw string(string(t)+":Needs another token instead "+m[curTok].value);
	}
	throwException(m[(int)m.size()-1].line_number, "Tokens ended");
	//itoa(m[(int)m.size()-1].line_number,t,10);
	//throw string(string(t)+":Tokens ended");
}

bool verify(string str){
	if(curTok<(int)m.size()){
		if(m[curTok].lexema==str){
			getNextToken();			
			return true;
		}
		return false;
	}
	return false;
}

ExprAST *expect(vector<ExprAST *(*)()>f){

	int i;
	//char t[30];
	int curTokCopy = getNumberToken();
	for(i=0; i<(int)f.size(); i++){
        try{
            return f[i]();
        }
        catch(string e){
            recoil(curTokCopy);
        }
	}
	//itoa(m[curTok].line_number,t,10);
	//throw string(string(t)+":Needs another token instead "+m[curTok].value);
	throwException(m[curTok].line_number, "Needs another token instead " + m[curTok].value);
	return NULL;
}

ExprAST *verify(vector<ExprAST *(*)()>f){

	int i;
	int curTokCopy = getNumberToken();
	for(i=0; i<(int)f.size(); i++){
        try{
            return f[i]();
        }
        catch(string e){
            recoil(curTokCopy);
        }
	}
	return NULL;
}

ExprAST *verify(ExprAST *(*f)()){

	int curTokCopy = getNumberToken();
        try{
            return f();
        }
        catch(string e){
            recoil(curTokCopy);
        }
	return NULL;
}


ExprAST *parseIntExpr(){
	int curTokCopy = getNumberToken();
	expect("int");
	return new IntExprAST(m[curTokCopy].recognized_value_int, m[curTokCopy].line_number);
}

ExprAST *parseRealExpr(){
	int curTokCopy = getNumberToken();
	expect("real");
	return new RealExprAST(m[curTokCopy].recognized_value_real, m[curTokCopy].line_number);
}

ExprAST *parseIDExpr(){
	int curTokCopy = getNumberToken();
	expect("id");
	return new IDExprAST(m[curTokCopy].value, m[curTokCopy].line_number);
}


ExprAST* parseVarExpr(){
	ExprAST *id, *index;
	id = parseIDExpr();

	if(!verify("lsb"))
		return new VarExprAST(id, NULL, id->getNumberLine());

	vector<ExprAST *(*)()>f;
	f.push_back(&parseIDExpr);
	f.push_back(&parseIntExpr);
	index=expect(f);
	
	expect("rsb");
	
	return new VarExprAST(id, index, id->getNumberLine());
}

ExprAST* parseLabelExpr(){
	ExprAST *id;
	id = parseIDExpr();

	expect("colon");

	return new LabelExprAST(id, id->getNumberLine());
}


ExprAST* parseGoToExpr(){
	expect("goto");
	
	ExprAST *id;
	id = parseIDExpr();
	return new GoToExprAST(id, id->getNumberLine());
}


ExprAST* parseReadExpr(){
	int curTokCopy = getNumberToken();
	expect("read");

	vector<ExprAST*> vars;
	do{
		ExprAST *var = parseVarExpr();
		vars.push_back(var);

	} while(verify("comma"));
	return new ReadExprAST(vars, m[curTokCopy].line_number);
}

ExprAST* parseOperandExpr(){
	if(verify("lrb")){
		ExprAST *l_operand = parseOperandExpr();
		ExprAST *r_operand = verify(&parseOperandExpr);
		
		if(r_operand!=NULL){
			int curTokCopy=getNumberToken();
			if(verify("add") ||  verify("min") || verify("mul") || verify("div") || verify("mod") || verify("eq") || verify("ne") || verify("lt") || verify("gt") || verify("le") || verify("ge")){
				expect("rrb");
				return new OperandExprAST(new BinaryExprAST(m[curTokCopy].lexema, l_operand, r_operand, l_operand->getNumberLine()), l_operand->getNumberLine());
			}
			else{
				//char t[30];
				//itoa(m[curTok].line_number,t,10);
				//throw string(string(t)+":Needs another token instead "+m[curTok].value);
				throwException(m[curTok].line_number, "Needs another token instead "+m[curTok].value);
			}
		}
		else{
			if(verify("min")){
				expect("rrb");
				return new OperandExprAST(new NegationExprAST(l_operand, l_operand->getNumberLine()), l_operand->getNumberLine());
			}
			else{
				expect("rrb");
				return l_operand;
			}
		}
	}
	else{
		vector<ExprAST *(*)()>f;
		f.push_back(&parseVarExpr);
		f.push_back(&parseIntExpr);
		f.push_back(&parseRealExpr);
		ExprAST *operand;
		operand = expect(f);
		return new OperandExprAST(operand, operand->getNumberLine());
	}
	return NULL;
}

ExprAST* parseMovExpr(){
	expect("lrb");
	ExprAST *expr, *var;
	expr = parseOperandExpr();
	var = parseVarExpr();
	expect("let");
	expect("rrb");
	return new MovExprAST(expr, var, expr->getNumberLine());
}


ExprAST* parseWriteExpr(){
	
	vector<ExprAST*> args;
	vector<bool> fl;
	int number_line = m[curTok].line_number;
	expect("write");
	do{
		int curTokCopy = getNumberToken();
		if(verify("skip") || verify("space") || verify("tab")){
			args.push_back(new QualifierExprAST(m[curTokCopy].lexema, m[curTokCopy].line_number));
			fl.push_back(false);
		}
		else{
			args.push_back(parseOperandExpr());
			fl.push_back(true);
		}
		
	}while(verify("comma"));

	return new WriteExprAST(args, fl, number_line);

}

ExprAST* parseBreakExpr(){
	int curTokCopy = getNumberToken();
	if(verify("break"))
		return new BreakExprAST(m[curTokCopy].line_number);
	throw string(m[curTok].line_number+":Expect break");

}

ExprAST* parseCallExpr(){
	ExprAST* name;
	vector<ExprAST*> args;
	name=parseIDExpr();
	expect("lrb");
	args.push_back(verify(&parseVarExpr));
	if(args[0]!=NULL){
		while(verify("comma")){
			args.push_back(parseVarExpr());
		}
	}
	else{
		args.pop_back();
	}
	
	expect("rrb");
	return new CallExprAST(name, args, name->getNumberLine());
}

ExprAST* parseTypeExpr(){
	int curTokCopy = getNumberToken();
	if(verify("typeint"))
		return new TypeExprAST("int", m[curTokCopy].line_number);
	if(verify("typereal"))
		return new TypeExprAST("real", m[curTokCopy].line_number);
	//char t[30];
	//itoa(m[curTok].line_number,t,10);
	//throw string(string(t)+":Expect type");
	throwException(m[curTok].line_number, "Expect type");
	return NULL;


}

ExprAST* parseDescrExpr(){
	ExprAST *type, *id, *num;
	vector<pair<ExprAST*, ExprAST*> > args;

	type=parseTypeExpr();

	do{
		id=parseIDExpr();
		if(verify("lsb")){
			int curTokCopy=getNumberToken();
			num = parseIntExpr();
			
			if(m[curTokCopy].recognized_value_int==0){
				//char t[30];
				//itoa(m[curTok].line_number,t,10);
				//throw string(string(t)+":Zero-length array");
				throwException(m[curTok].line_number, "Zero-length array");
			}
			expect("rsb");
			args.push_back(pair<ExprAST*, ExprAST*>(id, num));
		}
		else{
			args.push_back(pair<ExprAST*, ExprAST*>(id, NULL));
		}

	}while(verify("comma"));

	return new DescrExprAST(type, args, type->getNumberLine());



}

ExprAST* parseIfExpr(){
	ExprAST* expr;
	vector<ExprAST*> yes, no;
	int number_line = m[curTok].line_number;
	expect("if");
	expr = parseOperandExpr();
	expect("then");
	int curTokCopy;
	do{
		curTokCopy=getNumberToken();
		yes.push_back(verify(&parseOperatorExpr));
	}while(verify("semicolon"));
	recoil(curTokCopy);
	yes.pop_back();
	if((int)yes.size()==0){
		//char t[30];
		//itoa(m[curTok].line_number,t,10);
		//throw string(string(t)+":Operator needs");
		throwException(m[curTok].line_number, "Operator needs");
	}



	if(verify("else")){
		do{
			curTokCopy=getNumberToken();
			no.push_back(verify(&parseOperatorExpr));
		}while(verify("semicolon"));
		recoil(curTokCopy);
		no.pop_back();
		if((int)no.size()==0){
			//char t[30];
			//itoa(m[curTok].line_number,t,10);
			//throw string(string(t)+":Operator needs");
			throwException(m[curTok].line_number, "Operator needs");
		}
	}
	
	expect("end");
	return new IfExprAST(expr, yes, no, number_line);
}

ExprAST* parseCycleExpr(){
	vector<ExprAST*> body;
	int curTokCopy;
	int number_line = m[curTok].line_number;
	expect("loop");
	do{
		curTokCopy=getNumberToken();
		body.push_back(verify(&parseOperatorExpr));
		
	}while(verify("semicolon"));

	recoil(curTokCopy);
	body.pop_back();
	if((int)body.size()==0){
		//char t[30];
		//itoa(m[curTok].line_number,t,10);
		//throw string(string(t)+":Operator needs");
		throwException(m[curTok].line_number, "Operator needs");
	}
	expect("end");
	
	return new CycleExprAST(body, number_line);
}


ExprAST* parseComponentExpr(){
	vector<ExprAST*> body;
	int number_line = m[curTok].line_number;
	expect("beg");
	do{
		body.push_back(verify(&parseOperatorExpr));
	}while(verify("semicolon"));
	expect("end");
	return new ComponentExprAST(body, number_line);
}


ExprAST* parseOperatorExpr(){
	int number_line = m[curTok].line_number;
	ExprAST* label;
	ExprAST* unlabelled=NULL;
	label = verify(&parseLabelExpr);
	vector<ExprAST *(*)()>f;
	f.push_back(&parseComponentExpr);
	f.push_back(&parseMovExpr);
	f.push_back(&parseGoToExpr);
	f.push_back(&parseIfExpr);
	f.push_back(&parseCycleExpr);
	f.push_back(&parseReadExpr);
	f.push_back(&parseWriteExpr);
	f.push_back(&parseCallExpr);
	f.push_back(&parseBreakExpr);
	unlabelled = verify(f);
	return new OperatorExprAST(label, unlabelled, number_line);

}


ExprAST* parseFunction(){
	expect("procedure");
	ExprAST* id;
	vector<ExprAST*> args;
	id = parseIDExpr();
	int curTokCopy;

	do{
		curTokCopy=getNumberToken();
		args.push_back(verify(&parseDescrExpr));
		if(args[args.size()-1]==NULL)
			break;
	}while(verify("semicolon"));
	recoil(curTokCopy);
	args.pop_back();
	
	
	ExprAST* component = parseComponentExpr();
	return new FunctionAST(id, args, component, id->getNumberLine());
	
}


ExprAST* parseProgram(){
	vector<ExprAST*> vars;
	vector<ExprAST*> funs;
	ExprAST *body;
	if(verify("var")){
		do{
			vars.push_back(parseDescrExpr());
		}while(verify("semicolon"));
		
	}

	if(verify("implementation")){
		do{
			funs.push_back(parseFunction());
		}while(verify("semicolon"));
	}

	body = parseComponentExpr();
	return new ProgramAST(vars, funs, body);

}


ExprAST *getTree(vector<Lexema> v){
	if(setToken(v)){
		ExprAST *tree;
		try{
			tree = parseProgram();
		}
		catch(string e){
			cout<<"Error:"+e;
			return NULL;
		}
		if(curTok!=m.size())
			return NULL;
		return tree;
	}
	return NULL;
}

bool printTree(int num, ExprAST *t, ostream &out){
	if(t==NULL){
		if(num!=0)
			return false;
		out<<"<?xml version=\"1.0\" ?>"<<endl<<"<program>"<<endl<<"</program>"<<endl;
		return true;
	}
	out<<"<?xml version=\"1.0\" ?>"<<endl;
	t->printTag(out);
	return true;
};

void throwException(int number_line, string mas){
	char s[30];
	itoa(number_line, s, 10);
	throw string(string(s) + ":" + mas);
	return;
}

bool validityOfTree(int num, ExprAST *t){
	if(t==NULL){
		if(num!=0)
			return false;
		return true;
	}
	return true;
}