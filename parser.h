#ifndef parser_h
#define parser_h

#include <iostream>
#include <string>
#include <vector>
#include "lexer.h"
#include "grammar.h"
using namespace std;

bool setToken(vector<Lexema> v);

ExprAST* parseIntExpr();
ExprAST* parseRealExpr();
ExprAST* parseIDExpr();
ExprAST* parseVarExpr();
ExprAST* parseLabelExpr();
ExprAST* parseGoToExpr();
ExprAST* parseReadExpr();
ExprAST* parseOperandExpr();
ExprAST* parseMovExpr();
ExprAST* parseWriteExpr();
ExprAST* parseBreakExpr();
ExprAST* parseCallExpr();
ExprAST* parseTypeExpr();
ExprAST* parseDescrExpr();
ExprAST* parseIfExpr();
ExprAST* parseCycleExpr();
ExprAST* parseComponentExpr();
ExprAST* parseOperatorExpr();
ExprAST* parseFunction();
ExprAST* parseProgram();
ExprAST *getTree(vector<Lexema> v);
bool printTree(int num, ExprAST *t, ostream &out);
void throwException(int number_line, string mas);
bool validityOfTree(int num, ExprAST *t);


#endif




